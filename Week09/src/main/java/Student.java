import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_student")
public class Student implements Serializable {

    public Student() {
    }

    public Student(String name, String lastname, int age, String course, String classification) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.course = course;
        this.classification = classification;
    }



    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int id;
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }


    @Column(name = "name")
    private String name;
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    @Column(name = "lastname")
    private String lastname;
    public String getLastname() { return lastname; }
    public void setLastname(String lastname) { this.lastname = lastname; }

    @Column(name = "age")
    private int age;
    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }

    @Column(name = "course")
    private String course;
    public String getCourse() { return course; }
    public void setCourse(String course) { this.course = course; }

    @Column(name = "classification")
    private String classification;
    public String getClassification() { return classification; }
    public void setClassification(String classification) { this.classification = classification; }
}
