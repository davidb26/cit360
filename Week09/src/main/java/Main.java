import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    public static void main (String[] args) {
        Student student = new Student("David", "Bobadilla", 25, "Object Oriented Development", "Junior");
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(student);
        entityManager.getTransaction().commit();
        entityManagerFactory.close();
    }
}
