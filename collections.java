package David.Bobadilla;
//Import all the utilities I may need.
import java.util.*;

public class collections {

    public collections() {
    }
    public static void main(String[] args) {
        System.out.println("****My List collection****");
        //ArrayList() used to store dynamically sized collection of elements.
        List lista = new ArrayList();
        lista.add("Alfajores");
        lista.add("Chivito");
        lista.add("Mate");
        lista.add("Choripan");
        lista.add("Dulce de leche");
        lista.add("Facturas");
        lista.add("Asado");
        Iterator variable = lista.iterator();
        /*---------------------------*/
        while (variable.hasNext()) {
            Object pal = variable.next();
            System.out.println((String) pal);
        }

        System.out.println("****My Queue collection****");
        //PriorityQueue is employed when objects are supposed to be processed based on the priority.
        Queue fifo = new PriorityQueue();
        fifo.add("Tacos");
        fifo.add("Tlayudas");
        fifo.add("Henchiladas");
        fifo.add("Pozole");
        fifo.add("Birria");
        fifo.add("Pambazos");
        fifo.add("Flautas");
        fifo.add("Mole");
        Iterator iterator= fifo.iterator();
        /*-----------------------*/
        while(iterator.hasNext()) {
            System.out.println(fifo.poll());
        }

        System.out.println("****My Set collection****");
        //Java TreeSet class implements the Set interface that uses a tree for storage
        Set fijar = new TreeSet();
        fijar.add("Completos");
        fijar.add("Torta de Yogurt");
        fijar.add("Ajiaco");
        fijar.add("Chancho en piedra");
        fijar.add("Caldillo");
        fijar.add("Cazuela nogada");

        Iterator variable2 = fijar.iterator();
        /*---------------------------*/
        while (variable2.hasNext()) {
            Object pal2 = variable2.next();
            System.out.println((String) pal2);
        }


        System.out.println("****My Map collection****");
        //HashMap store key and value pair. Keys should be unique.
        Map mapa = new HashMap();
        mapa.put(1, "Gelato");
        mapa.put(2, "Lasagne");
        mapa.put(3, "Tiramisu");
        mapa.put(4, "Polenta");
        mapa.put(5, "Panini");
        mapa.put(6, "Antipasto");
        mapa.put(7, "Spaghetti");


        for(int fd = 1; fd <= 7; ++fd) {
            String resultado = (String)mapa.get(fd);
            System.out.println(resultado);
        }



        System.out.println("****My List using generics****");

        List<ubicacion> miLista = new LinkedList<ubicacion>();
        //LinkedList class is a collection which can contain many objects of same type like ArrayList.
        miLista.add(new ubicacion("Mexico", "Mexico City"));
        miLista.add(new ubicacion("Mexico", "Casas Grandes"));
        miLista.add(new ubicacion("Italy", "Arco"));
        miLista.add(new ubicacion("Italy", "Bolzano"));
        miLista.add(new ubicacion("Argentina", "Tigre"));
        miLista.add(new ubicacion("Argentina", "Escobar"));
        miLista.add(new ubicacion("Chile", "Achao"));

        for (ubicacion food : miLista) {
            System.out.println(food);
        }


    }
}
