//David Bobadilla
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MyJSON {

    public static String customerToJSON(customer customer) {

        ObjectMapper mapper = new ObjectMapper();
        String x = "";

        try {
            x = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return x;
    }

    public static customer JSONToCustomer(String s) {

        ObjectMapper mapper = new ObjectMapper();
        customer customer = null;

        try {
            customer = mapper.readValue(s, customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }

    public static void main(String[] args) {

        customer cust = new customer();
        cust.setID(9817 );

        cust.setName("David ");

        cust.setAddress("609 Trejo Street #422 Rexburg,Idaho ");
        cust.setPhone( 2084961411 );
        cust.setRole("Student ");
        cust.setCountry("Mexico ");
        cust.setAge("25 ");

        String json = MyJSON.customerToJSON(cust);
        System.out.println(json);

        customer cust2 = MyJSON.JSONToCustomer(json);
        System.out.println(cust2);
    }

}
