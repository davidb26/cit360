import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "/Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    //No matter if it is a get or post, they will be send to the proccessRequest method.
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
//No matter if it is a get or post, they will be send to the proccessRequest method.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String name = request.getParameter("user");
            String pass = request.getParameter("pass");

            if (name.equals("admin") && pass.equals("pass")) {
                response.sendRedirect("Welcome.jsp");
            } else {

                System.out.println("Only string and user authorized can login: ");
//                response.sendRedirect("index.jsp");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } finally {response.sendRedirect("index.jsp"); }
    }
}
