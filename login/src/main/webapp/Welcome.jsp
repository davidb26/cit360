<%--
  Created by IntelliJ IDEA.
  User: David Bobadilla
  Date: 10/31/2020
  Time: 2:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bienvenido</title>
</head>
<body>
<h1>Welcome to Home</h1>
<div>


    <h3>What Is a Servlet?</h3>
    <p>A <b>servlet</b> is a Java programming language class that is used to extend
    the capabilities of servers that host applications accessed by means of a request-response
    programming model. Although servlets can respond to any type of request, they are
    commonly used to extend the applications hosted by web servers. For such applications,
    Java Servlet technology defines HTTP-specific servlet classes.</p>

    <p> packages provide interfaces and classes for writing servlets. All
        servlets must implement the <tt>Servlet</tt> interface, which defines life-cycle methods. When implementing a generic
        service, you can use or extend the <tt>GenericServlet</tt> class provided with the
        Java Servlet API. The <tt>HttpServlet</tt> class provides methods, such as <tt>doGet</tt> and
        <tt>doPost</tt>, for handling HTTP-specific services.</p>

    <p>This chapter focuses on writing servlets that generate responses to HTTP requests. </p>


</div>
</body>
</html>
