//David Bobadilla -CIT360
import java.util.*;
public class exception {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        /*-------------*/
        //Here I get the first input with loop to avoid user entering 0's
        int a;
        do {
            System.out.print("Input the first number: ");
            a = input.nextInt();
        } while (a == 0);
        /*--------------*/
//I get the second input from user doing a do/while loop with try/catch inside to throw exception when 0 and have user try again.
        int b;
        Scanner input1 = new Scanner(System.in);
        do {
            System.out.print("Input the second number: ");
            b = input1.nextInt();
            try {
                if (b == 0) {
                    throw new ArithmeticException("Do not enter 0's Che! Try again");

                }
            }
            catch (ArithmeticException e) {
                System.out.println("Do not enter 0's Che! Try again");
            }
        } while (b == 0);
//I send a and b to the division method to do the operation
        int result = division(a, b);
        System.out.println("The division result is: " + result);
    }


    private static int division(int a, int b) {
        int operation = 0;
        operation = a / b;
        return operation;
//I return the operation made between a and b to main

    }


}








