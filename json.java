import com.google.gson.Gson;

public class json {
    static class Student{
        String name;
        public Student(String name) {
            this.name = name;

        }
    }
    /*
    //You run this first to convert the object and then
    //comment it out to use the next code to transform json to object.
   public static void main(String[] args) {
        Student student = new Student("David");
        Gson gson = new Gson();

        String json = gson.toJson(student);

        System.out.println(json);
    }
}
*/


    public static void main(String[] args) {
        String jsonText = "{\"name\":\"David\"}";
        Gson gson = new Gson();
        Student student = gson.fromJson(jsonText,Student.class);
        System.out.println(student.name);
    }


}
