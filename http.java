//David Bobadilla
import java.net.*;
import java.io.*;

public class http {

    public static void main(String[] args) {
        try {
            //This will read from the website
            URL website = new URL("http://jsonplaceholder.typicode.com/todos/1");
            //Here, I create the URL connection object.
            URLConnection content = website.openConnection();

            BufferedReader getPage = new BufferedReader(new InputStreamReader(content.getInputStream()));

            String httpCode;

            while ((httpCode = getPage.readLine()) != null) {
                if (httpCode.isEmpty() != true) {
                    System.out.println(httpCode);

                } else {
                    System.out.println("No code to display");
                }
            }

            for (int x = 1; x <= 8; ++x) {
                System.out.println(content.getHeaderFieldKey(x) + " = " + content.getHeaderField(x));
            }
            //I close the stream here.
            getPage.close();
        } catch (MalformedURLException e) {
            System.out.println("That was a bad URL:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOExeption: " + e.getMessage());
        }
    }

}