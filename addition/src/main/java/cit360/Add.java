package cit360;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Add", urlPatterns = {"/Add"})
public class Add extends HttpServlet {
// @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    public void processRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        int i = Integer.parseInt(request.getParameter("t1"));
        int j = Integer.parseInt(request.getParameter("t2"));

        int k = i + j;
        try (PrintWriter out = response.getWriter()) {
            out.println(k);
        }
    }
}
