//David Bobadilla

import org.junit.*;

import static org.junit.Assert.*;


import java.nio.channels.IllegalSelectorException;

import static org.junit.Assert.*;

public class junitdivisionex {
    public void divisionTest() {

    }

    @BeforeClass
    public static void setUpClass() throws Exception{
    }

    @AfterClass
    public static void tearDownClass() throws Exception{
    }

    @Before
    public void setUp() throws Exception{
    }

    @After
    public void tearDown() throws Exception{
    }


    // assertEquals()
//    @Test
//    public void divisionTestOne() {
//        try {
//            division newDivision = new division();
//            int e = newDivision.divisionTest(300, 15);
//            assertEquals(20, e);
//        } catch (Exception ex){
//            assertEquals("The number did not result equal",ex.getMessage());
//        }
//    }
    @Test
    public void divisionTestOne() {
        try {
            division newDivision = new division();
            int e = newDivision.divisionTest(300, 15);
            if (e != 20) {
                fail("Assert Equals not reached. Result does not equal 20");
            }
        } catch (Exception e) {
            assertEquals(20, e);
        }
    }

    // assertTrue()
//    @Test
//    public void divisionTestTwo() {
//        division newDivision = new division();
//        int e = newDivision.divisionTest(200, 10);
//        assertTrue(e == 20);
//    }
    // assertTrue()
    @Test
    public void divisionTestTwo() {
        try {
            division newDivision = new division();
            int e = newDivision.divisionTest(200, 10);
            if (e != 20) {
                fail("AssertTrue not met, The result is false");
            }
        } catch (Exception e) {
            assertTrue(true);
        }
    }


    // assertFalse()
//    @Test
//    public void divisionTestThree() {
//        division newDivision = new division();
//        int e = newDivision.divisionTest(100, 5);
//        assertFalse(e == 10);
//    }

        // assertFalse()
        @Test
        public void divisionTestThree() {
            try {
                division newDivision = new division();
                int e = newDivision.divisionTest(200, 5);
                if (e != 40) {
                    fail("AssertionFalse, the result is not 40");
                }
            } catch (Exception e) {
                assertFalse(false);
            }
        }


    // assertNotNull()
//    @Test
//    public void divisionTestFour() {
//        division newDivision = new division();
//        int e = newDivision.divisionTest(200, 5);
//        assertNotNull(e);
//    }

    @Test
    public void divisionTestFour() {
        try {
            division newDivision = new division();
            int e = newDivision.divisionTest(200, 5);
            if (e != 40) {
                fail("The result does not meet requirements");

            }
        } catch (Exception e) {
            assertNull(null);

        }
    }


    //assertSame()
    @Test
    public void divisionTestFive() {
        division newDivision = new division();
        int e = newDivision.divisionTest(300, 15);
        int x = newDivision.divisionTest(400, 20);
        assertSame(e, x);
    }



    //assertNotSame()
    @Test
    public void divisionTestSix() {
        division newDivision = new division();
        int e = newDivision.divisionTest(900, 5);
        int x = newDivision.divisionTest(350, 10);
        assertNotSame(e, x);
    }

    //assertArrayEquals()
    @Test
    public void divisionTestSeven() {
        division newDivision = new division();
        int [] e = new int[] {newDivision.divisionTest(400, 20)};
        int [] x = new int []{newDivision.divisionTest(800, 40)};
        assertArrayEquals(e, x);
    }

    //assertNull() I tried as many inputs I could but nothing worked. When looking
    //at the error, I found that this method always fail, so I am just commenting
    //it here to show you I tried this method.
//    @Test
//    public void testDivideTestEight() {
//        division newDivision = new division();
//        int i = newDivision.divisionTest(200, 5);
//        assertNull(i);
//    }
}

