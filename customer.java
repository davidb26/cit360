
public class customer {
    private Integer ID;
    private String name;
    private String address;
    private long phone;
    private String role;
    private String country;
    private String age;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    //
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    //
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    //
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }





    public String toString() {
        return "ID: " + ID + "Name: " + name + "Address: " + address + " Phone: " + phone +
                "Role: " + role + "Country: " + country + "Age: " + age;
    }
}