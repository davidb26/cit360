public class runnable implements Runnable{

    private Thread f;
    private String dish;

    //Use of a Runnable a class that implements the Runnable interface.
    runnable( String name)
    {
        dish = name;
        System.out.println("********************************************************************************");
        System.out.println("Welcome to Viva La Vaca! We received your " +  dish + " order and will be ready shortly.");
        System.out.println("********************************************************************************");
        System.out.println("Getting ingredients to make your " +  dish + "." );

    }
    public void run()
    {
        System.out.println("Combining " +  dish + " ingredients.");
        try {
            for(int w = 1; w > 0; w--) {
                System.out.println("Grilling your " + dish + "." );

                Thread.sleep(100);
            }
        }//Implementing InterruptedException.
        catch (InterruptedException e)
        {
            System.out.println("We ran out of ingredients for your" +  dish + " we apologize! Here is a free gift card.");
        }
        System.out.println("Your order of " +  dish + " is ready!");
    }

    public void start ()
    {
        System.out.println("Cooking your " +  dish + "." );
        if (f == null) {
            f = new Thread (this, dish);
            f.start ();
        }
    }
}
